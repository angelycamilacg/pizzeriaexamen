<%-- 
    Document   : opciones
    Created on : 21-dic-2020, 0:44:02
    Author     : CAMILA
--%>

<%@page import="DTO.Pizza"%>
<%@page import="DTO.Tipo"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mamma Mia</title>
        <meta name="description" content="">
        <meta name="author" content="">
        
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../fonts/font-awesome/css/font-awesome.css">
        
        <!-- Stylesheet
    ================================================== -->
        <link rel="stylesheet" type="text/css"  href="../css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rochester" rel="stylesheet">

    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Navigation
    ==========================================-->
    <nav id="menu" class="navbar navbar-default navbar-fixed-top">
        <div class="container"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="../index.html" class="page-scroll">Especialidades</a></li>
                    <li><a href="../index.html" class="page-scroll">Menú</a></li>
                    <li><a href="../index.html" class="page-scroll">Contactanos</a></li>
                    <li><a href="opciones.jsp" class="page-scroll">Ordena en linea</a></li>
                    <li><a class="page-scroll">Realizado Por: Camila Calderon</a></li>
                    <li><a style="margin-left: 860px" class="page-scroll">Código:1151763</a></li>
              

                </ul>
            </div>
            <!-- /.navbar-collapse --> 
        </div>
    </nav>

    <!-- Header -->
    <header id="header">
     <div class="intro">
      <div class="overlay">
       <div class="container">
        <div class="row">
         <div class="intro-text">
           <h1>Mamma Mia</h1>
            <p></p>
          </div>
          </div>
          </div>
        </div>
      </div>      
    </header>
    
    <!--Cantidad de pizzas-->
    <form action="../ControladorTamanos" method="get">
        <div id="features" class="text-center">
            <div class="container">

                <div class="section-title">
                    <h2>Digite cantidad de pizzas: </h2>
                </div>

                <div class="row">
                    <div class="col-sm">
                        <div class="features-item">
                            <input class="inpuut col-sm-3" type="number"  name="cantidad"/> 
                            <input type="submit" name="accion" value="crear"class="btn btn-warning" style="margin-right: 300px"/>                         
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </form>

 <!--tamanos-->
 <section>
     <div class="box">
         <%
             Object aux = request.getSession().getAttribute("cantidadPedida");
             if (aux != null) {
             int cantidad = Integer.parseInt(String.valueOf(request.getSession().getAttribute("cantidadPedida")));

         %>    
         <form action="../ControladorTamanos" method="get">   
             <% 
            for (int i = 1; i <= cantidad; i++) {           
             %>  
             
             <p class="tamano">Tamaño pizza <%=i%></p>
             <select class="form-control">
                 <%             
                  List<DTO.Tipo> tipoDTO = (List<DTO.Tipo>) (request.getSession().getAttribute("lista"));
                  for (DTO.Tipo t : tipoDTO) {
                 %> 

                 <option value="<%=t.getIdTipo()%>"><%=t.getDescripcion()%></option>

                 <%}%>
             </select>
             <%}%>
             <input type="submit" name="accion" value="cargar" class="btn btn-warning" style="margin-bottom: 100px; margin-left: 700px" />
         </form>
         <%} %>
     </div> 
 </section>

    <!-- Gallery Section -->
    <div id="gallery">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <div class="gallery-item"> <img src="../img/gallery/01.jpg" class="img-responsive" alt=""></div>
                </div>
                <div class="col-xs-6 col-md-3">
                    <div class="gallery-item"> <img src="../img/gallery/02.jpg" class="img-responsive" alt=""></div>
                </div>
                <div class="col-xs-6 col-md-3">
                    <div class="gallery-item"> <img src="../img/gallery/03.jpg" class="img-responsive" alt=""></div>
                </div>
                <div class="col-xs-6 col-md-3">
                    <div class="gallery-item"> <img src="../img/gallery/04.jpg" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>
    </div>


    <!-- Contact Section -->
    <div id="contact" class="text-center">
        <div class="container text-center">
            <div class="col-md-4">
                <h3>Reservaciones</h3>
                <div class="contact-item">
                    <p>Llame</p>
                    <p>(313) 351-2485</p>
                </div>
            </div>
            <div class="col-md-4">
                <h3>Dirección</h3>
                <div class="contact-item">
                    <p>4321 California St,</p>
                    <p>San Francisco, CA 12345</p>
                </div>
            </div>
            <div class="col-md-4">
                <h3>Horarios</h3>
                <div class="contact-item">
                    <p>Lunes-Martes: 10:00 AM - 11:00 PM</p>
                    <p>Viernes-Domingo: 11:00 AM - 02:00 AM</p>
                </div>
            </div>
        </div>
    </div>


    <div id="footer">
        <div class="container text-center">
            <div class="col-md-6">
                <p>&copy; 2017 Gusto. All rights reserved. Design by <a href="http://www.templatewire.com" rel="nofollow">TemplateWire</a></p>
            </div>
            <div class="col-md-6">
                <div class="social">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/SmoothScroll.js"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js/contact_me.js"></script> 
<script type="text/javascript" src="js/main.js"></script>              
</body>
</html>
