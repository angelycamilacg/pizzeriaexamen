package DTO;

import DTO.Pizza;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T00:22:10")
@StaticMetamodel(Tipo.class)
public class Tipo_ { 

    public static volatile SingularAttribute<Tipo, String> descripcion;
    public static volatile SingularAttribute<Tipo, Pizza> pizza;
    public static volatile SingularAttribute<Tipo, Integer> idTipo;

}