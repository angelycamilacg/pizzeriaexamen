package DTO;

import DTO.IngredienteAdicional;
import DTO.Pizza;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T00:22:10")
@StaticMetamodel(PizzaAdicional.class)
public class PizzaAdicional_ { 

    public static volatile SingularAttribute<PizzaAdicional, Integer> idpizzaAdicional;
    public static volatile SingularAttribute<PizzaAdicional, Pizza> idPizza;
    public static volatile SingularAttribute<PizzaAdicional, IngredienteAdicional> idIngrediente;
    public static volatile SingularAttribute<PizzaAdicional, Integer> idPizzaVenta;

}