package DTO;

import DTO.PizzaAdicional;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-22T00:22:09")
@StaticMetamodel(IngredienteAdicional.class)
public class IngredienteAdicional_ { 

    public static volatile SingularAttribute<IngredienteAdicional, String> descripcion;
    public static volatile SingularAttribute<IngredienteAdicional, Double> valor;
    public static volatile ListAttribute<IngredienteAdicional, PizzaAdicional> pizzaAdicionalList;
    public static volatile SingularAttribute<IngredienteAdicional, Integer> idIngrediente;

}