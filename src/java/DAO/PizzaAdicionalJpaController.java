/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.IngredienteAdicional;
import DTO.Pizza;
import DTO.PizzaAdicional;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author CAMILA
 */
public class PizzaAdicionalJpaController implements Serializable {

    public PizzaAdicionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PizzaAdicional pizzaAdicional) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            IngredienteAdicional idIngrediente = pizzaAdicional.getIdIngrediente();
            if (idIngrediente != null) {
                idIngrediente = em.getReference(idIngrediente.getClass(), idIngrediente.getIdIngrediente());
                pizzaAdicional.setIdIngrediente(idIngrediente);
            }
            Pizza idPizza = pizzaAdicional.getIdPizza();
            if (idPizza != null) {
                idPizza = em.getReference(idPizza.getClass(), idPizza.getIdPizza());
                pizzaAdicional.setIdPizza(idPizza);
            }
            em.persist(pizzaAdicional);
            if (idIngrediente != null) {
                idIngrediente.getPizzaAdicionalList().add(pizzaAdicional);
                idIngrediente = em.merge(idIngrediente);
            }
            if (idPizza != null) {
                idPizza.getPizzaAdicionalList().add(pizzaAdicional);
                idPizza = em.merge(idPizza);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PizzaAdicional pizzaAdicional) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PizzaAdicional persistentPizzaAdicional = em.find(PizzaAdicional.class, pizzaAdicional.getIdpizzaAdicional());
            IngredienteAdicional idIngredienteOld = persistentPizzaAdicional.getIdIngrediente();
            IngredienteAdicional idIngredienteNew = pizzaAdicional.getIdIngrediente();
            Pizza idPizzaOld = persistentPizzaAdicional.getIdPizza();
            Pizza idPizzaNew = pizzaAdicional.getIdPizza();
            if (idIngredienteNew != null) {
                idIngredienteNew = em.getReference(idIngredienteNew.getClass(), idIngredienteNew.getIdIngrediente());
                pizzaAdicional.setIdIngrediente(idIngredienteNew);
            }
            if (idPizzaNew != null) {
                idPizzaNew = em.getReference(idPizzaNew.getClass(), idPizzaNew.getIdPizza());
                pizzaAdicional.setIdPizza(idPizzaNew);
            }
            pizzaAdicional = em.merge(pizzaAdicional);
            if (idIngredienteOld != null && !idIngredienteOld.equals(idIngredienteNew)) {
                idIngredienteOld.getPizzaAdicionalList().remove(pizzaAdicional);
                idIngredienteOld = em.merge(idIngredienteOld);
            }
            if (idIngredienteNew != null && !idIngredienteNew.equals(idIngredienteOld)) {
                idIngredienteNew.getPizzaAdicionalList().add(pizzaAdicional);
                idIngredienteNew = em.merge(idIngredienteNew);
            }
            if (idPizzaOld != null && !idPizzaOld.equals(idPizzaNew)) {
                idPizzaOld.getPizzaAdicionalList().remove(pizzaAdicional);
                idPizzaOld = em.merge(idPizzaOld);
            }
            if (idPizzaNew != null && !idPizzaNew.equals(idPizzaOld)) {
                idPizzaNew.getPizzaAdicionalList().add(pizzaAdicional);
                idPizzaNew = em.merge(idPizzaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pizzaAdicional.getIdpizzaAdicional();
                if (findPizzaAdicional(id) == null) {
                    throw new NonexistentEntityException("The pizzaAdicional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PizzaAdicional pizzaAdicional;
            try {
                pizzaAdicional = em.getReference(PizzaAdicional.class, id);
                pizzaAdicional.getIdpizzaAdicional();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizzaAdicional with id " + id + " no longer exists.", enfe);
            }
            IngredienteAdicional idIngrediente = pizzaAdicional.getIdIngrediente();
            if (idIngrediente != null) {
                idIngrediente.getPizzaAdicionalList().remove(pizzaAdicional);
                idIngrediente = em.merge(idIngrediente);
            }
            Pizza idPizza = pizzaAdicional.getIdPizza();
            if (idPizza != null) {
                idPizza.getPizzaAdicionalList().remove(pizzaAdicional);
                idPizza = em.merge(idPizza);
            }
            em.remove(pizzaAdicional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PizzaAdicional> findPizzaAdicionalEntities() {
        return findPizzaAdicionalEntities(true, -1, -1);
    }

    public List<PizzaAdicional> findPizzaAdicionalEntities(int maxResults, int firstResult) {
        return findPizzaAdicionalEntities(false, maxResults, firstResult);
    }

    private List<PizzaAdicional> findPizzaAdicionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PizzaAdicional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PizzaAdicional findPizzaAdicional(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PizzaAdicional.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaAdicionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PizzaAdicional> rt = cq.from(PizzaAdicional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
