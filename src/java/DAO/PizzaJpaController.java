/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import DTO.Pizza;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.Tipo;
import DTO.Sabor;
import DTO.PizzaAdicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author CAMILA
 */
public class PizzaJpaController implements Serializable {

    public PizzaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pizza pizza) throws PreexistingEntityException, Exception {
        if (pizza.getPizzaAdicionalList() == null) {
            pizza.setPizzaAdicionalList(new ArrayList<PizzaAdicional>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tipo tipo = pizza.getTipo();
            if (tipo != null) {
                tipo = em.getReference(tipo.getClass(), tipo.getIdTipo());
                pizza.setTipo(tipo);
            }
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor = em.getReference(idSabor.getClass(), idSabor.getIdSabor());
                pizza.setIdSabor(idSabor);
            }
            List<PizzaAdicional> attachedPizzaAdicionalList = new ArrayList<PizzaAdicional>();
            for (PizzaAdicional pizzaAdicionalListPizzaAdicionalToAttach : pizza.getPizzaAdicionalList()) {
                pizzaAdicionalListPizzaAdicionalToAttach = em.getReference(pizzaAdicionalListPizzaAdicionalToAttach.getClass(), pizzaAdicionalListPizzaAdicionalToAttach.getIdpizzaAdicional());
                attachedPizzaAdicionalList.add(pizzaAdicionalListPizzaAdicionalToAttach);
            }
            pizza.setPizzaAdicionalList(attachedPizzaAdicionalList);
            em.persist(pizza);
            if (tipo != null) {
                Pizza oldPizzaOfTipo = tipo.getPizza();
                if (oldPizzaOfTipo != null) {
                    oldPizzaOfTipo.setTipo(null);
                    oldPizzaOfTipo = em.merge(oldPizzaOfTipo);
                }
                tipo.setPizza(pizza);
                tipo = em.merge(tipo);
            }
            if (idSabor != null) {
                idSabor.getPizzaList().add(pizza);
                idSabor = em.merge(idSabor);
            }
            for (PizzaAdicional pizzaAdicionalListPizzaAdicional : pizza.getPizzaAdicionalList()) {
                Pizza oldIdPizzaOfPizzaAdicionalListPizzaAdicional = pizzaAdicionalListPizzaAdicional.getIdPizza();
                pizzaAdicionalListPizzaAdicional.setIdPizza(pizza);
                pizzaAdicionalListPizzaAdicional = em.merge(pizzaAdicionalListPizzaAdicional);
                if (oldIdPizzaOfPizzaAdicionalListPizzaAdicional != null) {
                    oldIdPizzaOfPizzaAdicionalListPizzaAdicional.getPizzaAdicionalList().remove(pizzaAdicionalListPizzaAdicional);
                    oldIdPizzaOfPizzaAdicionalListPizzaAdicional = em.merge(oldIdPizzaOfPizzaAdicionalListPizzaAdicional);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPizza(pizza.getIdPizza()) != null) {
                throw new PreexistingEntityException("Pizza " + pizza + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pizza pizza) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza persistentPizza = em.find(Pizza.class, pizza.getIdPizza());
            Tipo tipoOld = persistentPizza.getTipo();
            Tipo tipoNew = pizza.getTipo();
            Sabor idSaborOld = persistentPizza.getIdSabor();
            Sabor idSaborNew = pizza.getIdSabor();
            List<PizzaAdicional> pizzaAdicionalListOld = persistentPizza.getPizzaAdicionalList();
            List<PizzaAdicional> pizzaAdicionalListNew = pizza.getPizzaAdicionalList();
            List<String> illegalOrphanMessages = null;
            if (tipoOld != null && !tipoOld.equals(tipoNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain Tipo " + tipoOld + " since its pizza field is not nullable.");
            }
            for (PizzaAdicional pizzaAdicionalListOldPizzaAdicional : pizzaAdicionalListOld) {
                if (!pizzaAdicionalListNew.contains(pizzaAdicionalListOldPizzaAdicional)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PizzaAdicional " + pizzaAdicionalListOldPizzaAdicional + " since its idPizza field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (tipoNew != null) {
                tipoNew = em.getReference(tipoNew.getClass(), tipoNew.getIdTipo());
                pizza.setTipo(tipoNew);
            }
            if (idSaborNew != null) {
                idSaborNew = em.getReference(idSaborNew.getClass(), idSaborNew.getIdSabor());
                pizza.setIdSabor(idSaborNew);
            }
            List<PizzaAdicional> attachedPizzaAdicionalListNew = new ArrayList<PizzaAdicional>();
            for (PizzaAdicional pizzaAdicionalListNewPizzaAdicionalToAttach : pizzaAdicionalListNew) {
                pizzaAdicionalListNewPizzaAdicionalToAttach = em.getReference(pizzaAdicionalListNewPizzaAdicionalToAttach.getClass(), pizzaAdicionalListNewPizzaAdicionalToAttach.getIdpizzaAdicional());
                attachedPizzaAdicionalListNew.add(pizzaAdicionalListNewPizzaAdicionalToAttach);
            }
            pizzaAdicionalListNew = attachedPizzaAdicionalListNew;
            pizza.setPizzaAdicionalList(pizzaAdicionalListNew);
            pizza = em.merge(pizza);
            if (tipoNew != null && !tipoNew.equals(tipoOld)) {
                Pizza oldPizzaOfTipo = tipoNew.getPizza();
                if (oldPizzaOfTipo != null) {
                    oldPizzaOfTipo.setTipo(null);
                    oldPizzaOfTipo = em.merge(oldPizzaOfTipo);
                }
                tipoNew.setPizza(pizza);
                tipoNew = em.merge(tipoNew);
            }
            if (idSaborOld != null && !idSaborOld.equals(idSaborNew)) {
                idSaborOld.getPizzaList().remove(pizza);
                idSaborOld = em.merge(idSaborOld);
            }
            if (idSaborNew != null && !idSaborNew.equals(idSaborOld)) {
                idSaborNew.getPizzaList().add(pizza);
                idSaborNew = em.merge(idSaborNew);
            }
            for (PizzaAdicional pizzaAdicionalListNewPizzaAdicional : pizzaAdicionalListNew) {
                if (!pizzaAdicionalListOld.contains(pizzaAdicionalListNewPizzaAdicional)) {
                    Pizza oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional = pizzaAdicionalListNewPizzaAdicional.getIdPizza();
                    pizzaAdicionalListNewPizzaAdicional.setIdPizza(pizza);
                    pizzaAdicionalListNewPizzaAdicional = em.merge(pizzaAdicionalListNewPizzaAdicional);
                    if (oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional != null && !oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional.equals(pizza)) {
                        oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional.getPizzaAdicionalList().remove(pizzaAdicionalListNewPizzaAdicional);
                        oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional = em.merge(oldIdPizzaOfPizzaAdicionalListNewPizzaAdicional);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pizza.getIdPizza();
                if (findPizza(id) == null) {
                    throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza pizza;
            try {
                pizza = em.getReference(Pizza.class, id);
                pizza.getIdPizza();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Tipo tipoOrphanCheck = pizza.getTipo();
            if (tipoOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pizza (" + pizza + ") cannot be destroyed since the Tipo " + tipoOrphanCheck + " in its tipo field has a non-nullable pizza field.");
            }
            List<PizzaAdicional> pizzaAdicionalListOrphanCheck = pizza.getPizzaAdicionalList();
            for (PizzaAdicional pizzaAdicionalListOrphanCheckPizzaAdicional : pizzaAdicionalListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pizza (" + pizza + ") cannot be destroyed since the PizzaAdicional " + pizzaAdicionalListOrphanCheckPizzaAdicional + " in its pizzaAdicionalList field has a non-nullable idPizza field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor.getPizzaList().remove(pizza);
                idSabor = em.merge(idSabor);
            }
            em.remove(pizza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pizza> findPizzaEntities() {
        return findPizzaEntities(true, -1, -1);
    }

    public List<Pizza> findPizzaEntities(int maxResults, int firstResult) {
        return findPizzaEntities(false, maxResults, firstResult);
    }

    private List<Pizza> findPizzaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pizza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pizza findPizza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pizza.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pizza> rt = cq.from(Pizza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
