/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLADOR;

import DAO.Conexion;
import DAO.TipoJpaController;
import DTO.IngredienteAdicional;
import DTO.Pizza;
import DTO.Sabor;
import DTO.Tipo;
import NEGOCIO.NegocioPizzeria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author CAMILA
 */
@WebServlet(name = "ControladorTamanos", urlPatterns = {"/ControladorTamanos"})
public class ControladorPizzeria extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControladorTamanos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControladorTamanos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    public void cantidad(HttpServletRequest request, HttpServletResponse response) throws IOException{   
     int cantidad = Integer.parseInt(request.getParameter("cantidad"));    
     NegocioPizzeria tamanos = new NegocioPizzeria(); 
     List<Tipo> tipoDTO = tamanos.getTipos();  
     
     request.getSession().setAttribute("cantidadPedida", cantidad); 
     request.getSession().setAttribute("lista", tipoDTO);
     
     response.sendRedirect("jsp/opciones.jsp");
    }

    
    public void opcionesDos(HttpServletRequest request, HttpServletResponse response) throws IOException{ 
      NegocioPizzeria sabor = new NegocioPizzeria();
      List<Sabor> saborDTO = sabor.getSabores();
      List<IngredienteAdicional> ingredienteDTO = sabor.getAdicionales();
      request.getSession().setAttribute("listaSabores", saborDTO);
      request.getSession().setAttribute("listaIngredientes", ingredienteDTO);
     response.sendRedirect("jsp/opciones2.jsp");
    }   

    
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        String action = request.getParameter("accion");

        if (action.equalsIgnoreCase("crear")) {
            cantidad(request, response);
        }
        if (action.equalsIgnoreCase("cargar")) {
            opcionesDos(request, response);
        }
        
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
