/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import DAO.IngredienteAdicionalJpaController;
import DAO.PizzaJpaController;
import DAO.SaborJpaController;
import DAO.TipoJpaController;
import DTO.IngredienteAdicional;
import DTO.Pizza;
import DTO.Sabor;
import DTO.Tipo;
import java.util.List;

/**
 *
 * @author CAMILA
 */
public class NegocioPizzeria {

    public NegocioPizzeria() {
    }
    
    public List<Pizza> getPizzas(){
    Conexion con = new Conexion();
    PizzaJpaController pizzaDAO = new PizzaJpaController(con.getBd());
    return pizzaDAO.findPizzaEntities();
    }
    
    public List<Tipo> getTipos(){
    Conexion con = new Conexion();
    TipoJpaController tipoDAO = new TipoJpaController(con.getBd());
    return tipoDAO.findTipoEntities();
    }
    
    public List<Sabor> getSabores(){
    Conexion con = new Conexion();
    SaborJpaController saborDAO = new SaborJpaController(con.getBd());
    System.out.println( saborDAO.findSaborEntities());
    return saborDAO.findSaborEntities();
    }
    
    public List<IngredienteAdicional> getAdicionales(){
    Conexion con = new Conexion();
    IngredienteAdicionalJpaController ingredienteDAO = new IngredienteAdicionalJpaController(con.getBd());
    return ingredienteDAO.findIngredienteAdicionalEntities();
    }
}
