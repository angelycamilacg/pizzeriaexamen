/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CAMILA
 */
@Entity
@Table(name = "pizza_adicional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PizzaAdicional.findAll", query = "SELECT p FROM PizzaAdicional p"),
    @NamedQuery(name = "PizzaAdicional.findByIdPizzaVenta", query = "SELECT p FROM PizzaAdicional p WHERE p.idPizzaVenta = :idPizzaVenta"),
    @NamedQuery(name = "PizzaAdicional.findByIdpizzaAdicional", query = "SELECT p FROM PizzaAdicional p WHERE p.idpizzaAdicional = :idpizzaAdicional")})
public class PizzaAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_pizza_venta")
    private int idPizzaVenta;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pizzaAdicional")
    private Integer idpizzaAdicional;
    @JoinColumn(name = "id_ingrediente", referencedColumnName = "id_ingrediente")
    @ManyToOne(optional = false)
    private IngredienteAdicional idIngrediente;
    @JoinColumn(name = "id_pizza", referencedColumnName = "id_pizza")
    @ManyToOne(optional = false)
    private Pizza idPizza;

    public PizzaAdicional() {
    }

    public PizzaAdicional(Integer idpizzaAdicional) {
        this.idpizzaAdicional = idpizzaAdicional;
    }

    public PizzaAdicional(Integer idpizzaAdicional, int idPizzaVenta) {
        this.idpizzaAdicional = idpizzaAdicional;
        this.idPizzaVenta = idPizzaVenta;
    }

    public int getIdPizzaVenta() {
        return idPizzaVenta;
    }

    public void setIdPizzaVenta(int idPizzaVenta) {
        this.idPizzaVenta = idPizzaVenta;
    }

    public Integer getIdpizzaAdicional() {
        return idpizzaAdicional;
    }

    public void setIdpizzaAdicional(Integer idpizzaAdicional) {
        this.idpizzaAdicional = idpizzaAdicional;
    }

    public IngredienteAdicional getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(IngredienteAdicional idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    public Pizza getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(Pizza idPizza) {
        this.idPizza = idPizza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpizzaAdicional != null ? idpizzaAdicional.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PizzaAdicional)) {
            return false;
        }
        PizzaAdicional other = (PizzaAdicional) object;
        if ((this.idpizzaAdicional == null && other.idpizzaAdicional != null) || (this.idpizzaAdicional != null && !this.idpizzaAdicional.equals(other.idpizzaAdicional))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.PizzaAdicional[ idpizzaAdicional=" + idpizzaAdicional + " ]";
    }
    
}
