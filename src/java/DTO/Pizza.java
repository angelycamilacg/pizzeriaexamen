/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author CAMILA
 */
@Entity
@Table(name = "pizza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pizza.findAll", query = "SELECT p FROM Pizza p"),
    @NamedQuery(name = "Pizza.findByIdPizza", query = "SELECT p FROM Pizza p WHERE p.idPizza = :idPizza"),
    @NamedQuery(name = "Pizza.findByIdTipo", query = "SELECT p FROM Pizza p WHERE p.idTipo = :idTipo"),
    @NamedQuery(name = "Pizza.findByValor", query = "SELECT p FROM Pizza p WHERE p.valor = :valor")})
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_pizza")
    private Integer idPizza;
    @Basic(optional = false)
    @Column(name = "id_tipo")
    private int idTipo;
    @Basic(optional = false)
    @Column(name = "valor")
    private double valor;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pizza")
    private Tipo tipo;
    @JoinColumn(name = "id_sabor", referencedColumnName = "id_sabor")
    @ManyToOne(optional = false)
    private Sabor idSabor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPizza")
    private List<PizzaAdicional> pizzaAdicionalList;

    public Pizza() {
    }

    public Pizza(Integer idPizza) {
        this.idPizza = idPizza;
    }

    public Pizza(Integer idPizza, int idTipo, double valor) {
        this.idPizza = idPizza;
        this.idTipo = idTipo;
        this.valor = valor;
    }

    public Integer getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(Integer idPizza) {
        this.idPizza = idPizza;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Sabor getIdSabor() {
        return idSabor;
    }

    public void setIdSabor(Sabor idSabor) {
        this.idSabor = idSabor;
    }

    @XmlTransient
    public List<PizzaAdicional> getPizzaAdicionalList() {
        return pizzaAdicionalList;
    }

    public void setPizzaAdicionalList(List<PizzaAdicional> pizzaAdicionalList) {
        this.pizzaAdicionalList = pizzaAdicionalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPizza != null ? idPizza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pizza)) {
            return false;
        }
        Pizza other = (Pizza) object;
        if ((this.idPizza == null && other.idPizza != null) || (this.idPizza != null && !this.idPizza.equals(other.idPizza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Pizza[ idPizza=" + idPizza + " ]";
    }
    
}
